import os
import sys

import requests
from datetime import datetime

from termcolor import cprint


# получаем информацию о пользователе
def get_user_info(user_info):
    user_id = user_info['id']
    name = user_info['name']
    username = user_info['username']
    email = user_info['email']
    company_name = user_info['company']['name']
    return company_name, email, name, user_id, username


class UsersTasks:

    def __init__(self, users_link, tasks_link, out_path):
        self.completed_tasks = []
        self.uncompleted_tasks = []
        self.users_link = users_link
        self.tasks_link = tasks_link
        if not os.path.exists(out_path):
            os.makedirs('tasks')
        self.out_path = out_path
        self.users_id_set = ()

    # получаем задания для определенного юзера
    def get_tasks_for_user(self, tasks_data, user_id):
        if not self.users_id_set:
            self.get_all_users_id(tasks_data)
        for task_info in tasks_data:
            if user_id in self.users_id_set:
                if 'title' in task_info and 'userId' in task_info:
                    if task_info['userId'] == user_id:
                        task_condition = task_info['completed']
                        task_name = task_info['title']
                        self.append_task(task_condition, task_name)
            else:
                print(f'Для пользователя {id} нет заданий')
                return False
        return True

    # получаем id всех пользователей
    def get_all_users_id(self, tasks_data):
        users_id_list = []
        for task_info in tasks_data:
            if 'userId' in task_info:
                users_id_list.append(task_info['userId'])
        self.users_id_set = set(users_id_list)

    #  добавляем задания в списко для пользователя
    def append_task(self, task_condition, task_name):
        if len(task_name) > 50:
            task_name = task_name[:50] + '...'
        if task_condition:
            self.completed_tasks.append(task_name)
        else:
            self.uncompleted_tasks.append(task_name)

    # пытаемся записать в файл
    def write_tasks_into_file(self, company_name, current_time, email, full_file_path, name, have_tasks):
        try:
            with open(full_file_path, mode='w', encoding='utf8') as file:
                self.set_data_to_write(company_name, current_time, email, file, have_tasks, name)
        except Exception:
            cprint('При попытке записать данные в файл возникла ошибка!', color='red')
            sys.exit(1)

    # выбираем данные для записи в файл
    def set_data_to_write(self, company_name, current_time, email, file, have_tasks, name):
        if have_tasks:
            file.write(f"{name} <{email}> {current_time}\n")
            file.write(f'{company_name}\n')
            file.write('\nЗавершенные задачи:\n')
            for task in self.completed_tasks:
                file.write(f'{task}\n')
            file.write('\nОставшиеся задачи:\n')
            for task in self.uncompleted_tasks:
                file.write(f'{task}\n')
        else:
            file.write(f"{name} <{email}> {current_time}\n")
            file.write(f'{company_name}\n')
            file.write(f'Для юзера {name} нет задач')

    # переименовываем файл
    def rename_file(self, full_file_path, username):
        file_create_time = os.path.getmtime(full_file_path)
        normalize_file_create_time = datetime.fromtimestamp(file_create_time).strftime("%Y-%m-%dT%H-%M")
        new_file_name = os.path.join(self.out_path, username) + '_' + \
            normalize_file_create_time + '.txt'
        os.rename(full_file_path, new_file_name)

    def run(self):
        try:
            users = requests.get(self.users_link)
            tasks = requests.get(self.tasks_link)
        except requests.exceptions.RequestException as exc:
            cprint('При попытке получить данные возникла ошибка!', color='red')
            print(exc)
            sys.exit(1)

        user_data = users.json()
        for user_info in user_data:
            if 'name' in user_info:
                company_name, email, name, user_id, username = get_user_info(user_info)
                now = datetime.now()
                current_time = now.strftime("%d.%m.%Y %H:%M")

                tasks_data = tasks.json()
                have_tasks = self.get_tasks_for_user(tasks_data, user_id)
                full_file_path = os.path.join(self.out_path, username) + '.txt'
                # если файл уже существует - переименовываем и создаем актуальный
                if os.path.isfile(full_file_path):
                    self.rename_file(full_file_path, username)
                    self.write_tasks_into_file(company_name, current_time, email, full_file_path, name, have_tasks)
                # иначе, просто создаем новые
                else:
                    self.write_tasks_into_file(company_name, current_time, email, full_file_path, name, have_tasks)
            self.completed_tasks.clear()
            self.uncompleted_tasks.clear()


def main():
    users_tasks = UsersTasks(users_link='https://json.medrating.org/users',
                             tasks_link='https://json.medrating.org/todos',
                             out_path='tasks')
    users_tasks.run()


if __name__ == '__main__':
    main()
